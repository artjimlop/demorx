package com.demoproject.mobile.androidtest.app

import android.app.Application
import com.example.userlist.di.DATABASE_NAME
import com.example.userlist.di.Endpoints
import com.example.userlist.di.RETROFIT_ENDPOINT
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
open class App: Application() {
    override fun onCreate() {
        setupEndpoints()
        super.onCreate()
    }

     open fun setupEndpoints() {
        Endpoints.retrofitEndpoint = RETROFIT_ENDPOINT
        Endpoints.databaseName = DATABASE_NAME
    }
}