package com.demoproject.mobile.androidtest.app

import com.example.userdetail.di.userDetailModules
import com.example.userlist.di.userListModules
import org.koin.core.KoinApplication

fun KoinApplication.appFeaturesModules() {
    modules(userListModules)
    modules(userDetailModules)
}