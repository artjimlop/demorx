package com.demoproject.mobile.androidtest.di

import com.example.userlist.di.*

val testModules = listOf(
    viewModelModule,
    repositoryModule,
    testNetModule,
    apiModule,
    testDatabaseModule
)