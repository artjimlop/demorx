package com.demoproject.mobile.androidtest.app

import com.example.userlist.di.Endpoints
import com.example.userlist.di.TEST_DATABASE_NAME
import com.example.userlist.di.TEST_ENDPOINT

class TestApp: App() {
    override fun setupEndpoints() {
        Endpoints.retrofitEndpoint = TEST_ENDPOINT
        Endpoints.databaseName = TEST_DATABASE_NAME
    }
}