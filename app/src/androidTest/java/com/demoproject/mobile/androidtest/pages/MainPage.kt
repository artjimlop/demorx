package com.demoproject.mobile.androidtest.pages

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.demoproject.mobile.androidtest.R

class MainPage {
    fun clickOnFirstUser() {
        onView(withId(R.id.userList)).perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ViewActions.click()))
    }
}