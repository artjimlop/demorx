package com.example.userdetail

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.userdetail.viewmodel.UserDetailViewModel
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_user_detail.*

@AndroidEntryPoint
class UserDetailActivity : AppCompatActivity() {

    private val userViewModel: UserDetailViewModel by viewModels()

    private val id: Long
        get() = intent.extras?.getLong(ID) ?: throw IllegalArgumentException("The ID parameter can not be null.")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)
        userViewModel.githubUser.observe(this, Observer {
            it?.let {
                login.text = it.login
                Picasso.get().load(it.avatar_url).into(avatar)
            }
        })
        userViewModel.loadUser(id)
    }
}