package com.example.userdetail.di

import com.example.navigation.UserDetailNavigator
import com.example.userdetail.UserDetailNavigation
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class UserDetailFeatureModule {

    @Singleton
    @Provides
    fun provideNavigation(): UserDetailNavigator = UserDetailNavigation()
}