package com.example.userdetail.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.core.UserRepository
import com.example.core.entity.GithubUser
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.schedulers.Schedulers

class UserDetailViewModel @ViewModelInject constructor(private val userRepository: UserRepository) : ViewModel() {

    private val _githubUser = MutableLiveData<GithubUser>()
    val githubUser: LiveData<GithubUser>
        get() = _githubUser

    fun loadUser(id: Long) {
        userRepository.getUser(id).subscribeOn(Schedulers.io())
            .observeOn(mainThread()).subscribe { user ->
                _githubUser.value = user
            }
    }
}