package com.example.userdetail

import android.content.Context
import android.content.Intent
import com.example.navigation.UserDetailNavigator

class UserDetailNavigation() : UserDetailNavigator {

    override fun goToUserDetail(context: Context, id: Long) {
        with(context) {
            startActivity(
                Intent(this, UserDetailActivity::class.java).apply {
                    putExtra(ID, id)
                }
            )
        }
    }
}

const val ID = "id"