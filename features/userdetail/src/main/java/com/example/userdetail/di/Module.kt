package com.example.userdetail.di

import com.example.navigation.UserDetailNavigator
import com.example.userdetail.UserDetailNavigation
import com.example.userdetail.viewmodel.UserDetailViewModel
import org.koin.dsl.module

val userDetailNavigationModule = module {
    fun provideNavigation(): UserDetailNavigator {
        return UserDetailNavigation()
    }

    single { provideNavigation() }
}

val viewModelModule = module {
    single { UserDetailViewModel(get()) }
}