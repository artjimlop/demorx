package com.example.userdetail.viewmodel

import com.example.core.UserRepository
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Test

@ExperimentalCoroutinesApi
class UserDetailViewModelTest : com.example.core_testing.BaseViewModelTest() {
    private lateinit var viewModel: UserDetailViewModel
    private val userRepository: UserRepository = mockk(relaxed = true, relaxUnitFun = true)

    override fun onPrepareTest() {
        viewModel = UserDetailViewModel(userRepository)
    }

    @Test
    fun `WHEN load user is called, THEN user should be retrieved from the repository`() = runBlocking {
        viewModel.loadUser(ID)
        verify { userRepository.getUser(ID) }
    }
}

private const val ID = 0L