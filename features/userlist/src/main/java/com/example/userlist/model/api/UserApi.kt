package com.example.userlist.model.api

import com.example.core.entity.GithubUser
import io.reactivex.Single
import retrofit2.http.GET

interface UserApi {

    @GET("users")
    fun getAllAsync(): Single<List<GithubUser>>
}