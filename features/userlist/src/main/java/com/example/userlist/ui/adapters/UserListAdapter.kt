package com.example.userlist.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.core.entity.GithubUser
import com.example.userlist.R

class UserListAdapter(private val onItemSelectedListener: (GithubUser) -> Unit) : RecyclerView.Adapter<UserItemViewHolder>() {

    private lateinit var users: List<GithubUser>

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): UserItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.user_item, parent, false)
        return UserItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserItemViewHolder, position: Int) {
        holder.bind(users[position], onItemSelectedListener)
    }

    override fun getItemCount() = users.size

    fun updateUsers(users: List<GithubUser>) {
        this.users = users
    }
}