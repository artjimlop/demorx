package com.example.userlist.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.core.entity.GithubUser
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface UserDao {

    @Query("SELECT * FROM users")
    fun findAll(): Flowable<List<GithubUser>>

    @Query("SELECT * from users where id = :id LIMIT 1")
    fun findUser(id: Long): Single<GithubUser>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(users: List<GithubUser>): Completable
}