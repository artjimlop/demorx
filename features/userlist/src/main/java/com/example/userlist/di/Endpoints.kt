package com.example.userlist.di

object Endpoints {
    lateinit var retrofitEndpoint: String
    lateinit var databaseName: String
}

const val RETROFIT_ENDPOINT = "https://api.github.com/"
const val DATABASE_NAME = "eds.database"

const val TEST_ENDPOINT = "http://localhost:8080/"
const val TEST_DATABASE_NAME = "test.eds.database"