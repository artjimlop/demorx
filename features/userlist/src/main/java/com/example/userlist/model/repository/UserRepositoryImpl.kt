package com.example.userlist.model.repository

import com.example.core.UserRepository
import com.example.userlist.model.api.UserApi
import com.example.userlist.model.dao.UserDao
import io.reactivex.Completable
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(private val userApi: UserApi, private val userDao: UserDao): UserRepository {

    override fun getUsers() = userDao.findAll()

    override fun refresh(): Completable {
        return userApi.getAllAsync()
            .flatMapCompletable {
                    users -> userDao.add(users)
            }
    }

    override fun getUser(id: Long) = userDao.findUser(id)
}