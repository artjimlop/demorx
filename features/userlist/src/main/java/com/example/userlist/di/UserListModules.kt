package com.example.userlist.di

val userListModules = listOf(
    viewModelModule,
    repositoryModule,
    netModule,
    apiModule,
    databaseModule
)