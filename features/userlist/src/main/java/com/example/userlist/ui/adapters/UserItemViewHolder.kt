package com.example.userlist.ui.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.core.entity.GithubUser
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.user_item.view.*

class UserItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(user: GithubUser, onItemClicked: (GithubUser) -> Unit) {
        Picasso.get().load(user.avatar_url).into(itemView.avatar)
        itemView.login.text = user.login
        itemView.setOnClickListener {
            onItemClicked(user)
        }
    }
}