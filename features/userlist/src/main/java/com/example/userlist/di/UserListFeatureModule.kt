package com.example.userlist.di

import android.app.Application
import androidx.room.Room
import com.example.core.UserRepository
import com.example.userlist.di.Endpoints.databaseName
import com.example.userlist.di.Endpoints.retrofitEndpoint
import com.example.userlist.model.AppDatabase
import com.example.userlist.model.api.UserApi
import com.example.userlist.model.dao.UserDao
import com.example.userlist.model.repository.UserRepositoryImpl
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class UserListFeatureModule {

    @Singleton
    @Provides
    fun provideRetrofit(factory: Gson, client: OkHttpClient): UserApi {
        return Retrofit.Builder()
            .baseUrl(retrofitEndpoint)
            .addConverterFactory(GsonConverterFactory.create(factory))
            //.addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()
            .create(UserApi::class.java)
    }

    @Singleton
    @Provides
    fun provideCache(application: Application): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(application.cacheDir, cacheSize.toLong())
    }

    @Singleton
    @Provides
    fun provideHttpClient(cache: Cache): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()
            .cache(cache)

        okHttpClient = okHttpClientBuilder.build()
        return okHttpClient
    }

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create()
    }

    @Singleton
    @Provides
    fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, databaseName)
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }

    @Singleton
    @Provides
    fun provideDao(database: AppDatabase): UserDao {
        return database.userDao
    }

    @Singleton
    @Provides
    fun provideUserRepository(api: UserApi, dao: UserDao): UserRepository {
        return UserRepositoryImpl(api, dao)
    }
}