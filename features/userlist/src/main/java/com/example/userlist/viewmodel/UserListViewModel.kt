package com.example.userlist.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.core.UserRepository
import com.example.core.entity.GithubUser
import com.example.userlist.utils.LoadingState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class UserListViewModel @ViewModelInject constructor(private val userRepository: UserRepository) :
    ViewModel() {

    private val _loadingState = MutableLiveData<LoadingState>()
    val loadingState: LiveData<LoadingState>
        get() = _loadingState

    private val _data = MutableLiveData<List<GithubUser>>()
    val data: LiveData<List<GithubUser>>
        get() = _data

    init {
        fetchData()
        loadUserList()
    }

    private fun fetchData() {
        _loadingState.value = LoadingState.LOADING
        userRepository.refresh()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    _loadingState.value = LoadingState.LOADED
                }, {
                    _loadingState.value = LoadingState.error(it.message)
                })
    }

    private fun loadUserList() {
        userRepository.getUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { userList ->
                _data.value = userList
            }
    }
}