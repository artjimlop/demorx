package com.example.userlist

import com.example.core.UserRepository
import com.example.core.entity.GithubUser
import com.example.userlist.viewmodel.UserListViewModel
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Flowable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Test

@ExperimentalCoroutinesApi
class UserListViewModelTest: com.example.core_testing.BaseViewModelTest() {
    private lateinit var viewModel: UserListViewModel
    private val userRepository: UserRepository = mockk(relaxed = true, relaxUnitFun = true)
    private val users: Flowable<List<GithubUser>> = Flowable.just(emptyList())

    override fun onPrepareTest() {
        viewModel = UserListViewModel(userRepository)
        every { userRepository.getUsers() } answers { users }
        every { userRepository.refresh() } answers { Completable.complete() }
    }

    @Test
    fun `WHEN viewmodel is initialized, THEN data should be refreshed`() {
        verify {
            userRepository.refresh()
            userRepository.getUsers()
        }
    }
}