package com.example.userlist.model.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.core.UserRepository
import com.example.core.entity.GithubUser
import com.example.core_testing.RxImmediateScheduler
import com.example.userlist.model.api.UserApi
import com.example.userlist.model.dao.UserDao
import io.mockk.*
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class UserRepositoryImplTest {

    private lateinit var repository: UserRepository
    private val userDao: UserDao = mockk(relaxed = true)
    private val userApi: UserApi = mockk()
    private val user = GithubUser(ID, "login", "avatar_url")

    @get:Rule
    val rxSchedulerRule = RxImmediateScheduler()
    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        repository = UserRepositoryImpl(userApi, userDao)
    }

    @Test
    fun `WHEN refreshing users from API, THEN add them to DB`() {
        val users = listOf(user)
        every { userApi.getAllAsync() } answers { Single.just(users) }

        repository.refresh()

        verify {
            userApi.getAllAsync()
            userDao.add(users)
        }
    }


    @Test
    fun `WHEN getting user by id, THEN find it on the DB`() {
        repository.getUser(ID)

        verify {
            userDao.findUser(ID)
        }
    }

    @Test
    fun `WHEN loading data, THEN load it from the DB`() {
        repository.getUsers()

        verify {
            userDao.findAll()
        }
    }
}

private const val ID = 0L