package com.example.core_testing

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.mockito.MockitoAnnotations
import java.util.concurrent.Callable


@ExperimentalCoroutinesApi
abstract class BaseViewModelTest {
    @get:Rule
    val rxSchedulerRule = RxImmediateScheduler()
    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    @Before
    open fun setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler: Callable<Scheduler?>? -> Schedulers.trampoline() }
        MockitoAnnotations.initMocks(this)
        //Dispatchers.setMain(mainThreadSurrogate)
        onPrepareTest()
    }

    abstract fun onPrepareTest()
}