package com.example.core

import com.example.core.entity.GithubUser
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface UserRepository {
    fun refresh(): Completable
    fun getUsers(): Flowable<List<GithubUser>>
    fun getUser(id: Long): Single<GithubUser>
}